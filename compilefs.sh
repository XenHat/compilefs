#!/bin/bash

#### Compilation and automation script for Phoenix-Firestorm-LGPL Second Life Viewer.
#### Free to modify/reuse/edit as long as you mention the original author, David Turenne.
#### Revision 1.015 | Wed Nov 2 23:53:31 EDT 2011
### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.
###
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.
###
### You should have received a copy of the GNU General Public License
### along with this program. If not, see <http://www.gnu.org/licenses/>.
function check() {
		if [[ -z `pgrep autobuild` ]]; then echo "Starting to compile...";
else echo "autobuild is already running. Won't do.";exit 1;fi
		} 
function unpack() { # creates an usable folder in $HOME, same as running install.sh
cd $HOME; # safe
rm -rf $HOME/firestorm/; # remove the old one
                        # unpack latest compiled tarball
tar -C $HOME -xaf `ls $cD/*.tar.bz2 -t1 | head -n1`;
                        # be sure we don't move some random newly-modified file
mv `ls $HOME | grep Phoenix_Firestorm | head -n1` $HOME/firestorm ;
echo "Done.";
}
# Directories
vT="phoenix-firestorm-lgpl"
bD="$HOME/src/viewers/${vT}" # Build directory, for reuse
cD="$bD/build-linux-i686/newview" # compiled directory, for reuse
# Commands
cK='autobuild build -c ReleaseFS_open -- --kdu --package' # KDU
cO='autobuild build -c ReleaseFS_open -- --package' # OpenJPEG
cd $bD
case "$1" in
kdu|-k|--kdu)
hg pull -u # Update to latest rev
hg update # Update it... TODO: stop if merge fail.
check
autobuild configure -c ReleaseFS_open -- --kdu --package # needed to configure it once for next step
if [ -a $bD/build-linux-i686/packages/lib/release/libkdu.a ]; then # was the kdu decoder properly found ?
check
$cK && unpack ; # Yep.
else
echo "No llkdu source found."; # Nope.
fi
;;
unpack|-u|--unpack) # Does not pull, just unpack
unpack;
;;
open|-o|--open|--jpeg|--openjpeg)
hg pull -u
hg update
check
$cO && unpack;
;;
--help) # How to use
echo "
Usage: compilefs [COMMAND]
Compiles the Firestorm Second Life viewer
Commands are:
kdu,-k Compile the viewer with the Kakadu JPEG2000 decoder
open,-o Compile the viewer with the OpenJPEG JPEG2000 decoder
unpack,-u Unpack a previously compiled viewer
clear,-c Clear the build dir (before starting from scratch)
"
;;
"")
echo "compilefs: missing parameter
try 'compilefs --help' for more information"
exit 3
;;
clear|-c)
rm -rf $bD/build-linux-i686
;;
*)
echo "compilefs: invalid parameter
try 'compilefs --help' for more information"
exit 4
esac
unset cD
unset bD
unset vT
unset cO
unset cKx
exit $?
